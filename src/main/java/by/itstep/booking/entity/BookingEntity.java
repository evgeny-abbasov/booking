package by.itstep.booking.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "booking")
@Where(clause = "deleted_at IS NULL")
public class BookingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "date_in")
    private Instant dateIn;

    @Column(name = "date_out")
    private Instant dateOut;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "price")
    private Double price;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "booking", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserEntity> users = new ArrayList<>();

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "booking", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RoomEntity> rooms = new ArrayList<>();


}
